/****************************************************************
 *								*
 * Copyright (c) 2019-2020 YottaDB LLC and/or its subsidiaries.	*
 * All rights reserved.						*
 *								*
 *	This source code contains the intellectual property	*
 *	of its copyright holder(s), and is made available	*
 *	under a license.  If you do not know the terms of	*
 *	the license, please stop and do not read further.	*
 *								*
 ****************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "octo.h"
#include "octo_types.h"

// Helper function that is invoked when we have to traverse a "column_list_alias_STATEMENT".
// Caller passes "do_loop" variable set to TRUE  if they want us to traverse the linked list.
//                              and set to FALSE if they want us to traverse only the first element in the linked list.
int populate_data_type_column_list_alias(SqlStatement *v, SqlValueType *type, boolean_t do_loop, char *cursorId) {
	SqlColumnListAlias	*column_list_alias, *cur_column_list_alias;
	SqlValueType		child_type1;
	int			result = 0;

	*type = UNKNOWN_SqlValueType;
	if ((NULL != v) && (NULL != v->v.select))
	{
		// SqlColumnListAlias
		UNPACK_SQL_STATEMENT(column_list_alias, v, column_list_alias);
		cur_column_list_alias = column_list_alias;
		do {
			child_type1 = UNKNOWN_SqlValueType;
			// SqlColumnList
			result |= populate_data_type(cur_column_list_alias->column_list, &child_type1, cursorId);
			if (UNKNOWN_SqlValueType == cur_column_list_alias->type) {
				cur_column_list_alias->type = child_type1;
			} else if (cur_column_list_alias->type != child_type1) {
				// This is currently possible if a column is explicitly typecast
				// Disable the below code until #304 is fixed at which point it is possible
				//	this code can be re-enabled.
				// assert(FALSE);
				// result |= 1;
			}
			*type = child_type1;
			cur_column_list_alias = cur_column_list_alias->next;
		} while (do_loop && (cur_column_list_alias != column_list_alias));
	}
	return result;
}

// Helper function that is invoked when we have to traverse a "column_list_STATEMENT".
// Caller passes "do_loop" variable set to TRUE  if they want us to traverse the linked list.
//                              and set to FALSE if they want us to traverse only the first element in the linked list.
int populate_data_type_column_list(SqlStatement *v, SqlValueType *type, boolean_t do_loop, char *cursorId) {
	SqlColumnList	*column_list, *cur_column_list;
	int		result = 0;

	*type = UNKNOWN_SqlValueType;
	if ((NULL != v) && (NULL != v->v.select))
	{
		// SqlColumnList
		UNPACK_SQL_STATEMENT(column_list, v, column_list);
		cur_column_list = column_list;
		do {
			// SqlValue or SqlColumnAlias
			result |= populate_data_type(cur_column_list->value, type, cursorId);
			cur_column_list = cur_column_list->next;
		} while (do_loop && (cur_column_list != column_list));
	}
	return result;
}

int populate_data_type(SqlStatement *v, SqlValueType *type, char *cursorId) {
	SqlBinaryOperation	*binary = NULL;
	SqlCaseBranchStatement	*cas_branch, *cur_branch;
	SqlCaseStatement	*cas;
	SqlColumn		*column = NULL;
	SqlFunctionCall		*function_call;
	SqlAggregateFunction	*aggregate_function;
	SqlJoin			*start_join, *cur_join;
	SqlSetOperation		*set_operation;
	SqlTableAlias		*table_alias;
	SqlUnaryOperation	*unary = NULL;
	SqlValue		*value = NULL;
	SqlValueType		child_type1, child_type2;
	YYLTYPE			location;
	SqlSelectStatement	*select;
	int			result = 0;

	*type = UNKNOWN_SqlValueType;
	if (v == NULL || v->v.select == NULL)
		return 0;
	// Note: The below switch statement and the flow mirrors that in hash_canonical_query.c.
	//       Any change here or there needs to also be done in the other module.
	switch(v->type) {
	case cas_STATEMENT:
		UNPACK_SQL_STATEMENT(cas, v, cas);
		// We expect type to get overriden here; only the last type matters
		result |= populate_data_type(cas->value, type, cursorId);
		result |= populate_data_type(cas->branches, type, cursorId);
		result |= populate_data_type(cas->optional_else, type, cursorId);
		break;
	case cas_branch_STATEMENT:
		UNPACK_SQL_STATEMENT(cas_branch, v, cas_branch);
		cur_branch = cas_branch;
		do {
			result |= populate_data_type(cur_branch->condition, type, cursorId);
			// TODO: we should check type here to make sure all branches have the same type
			result |= populate_data_type(cur_branch->value, type, cursorId);
			cur_branch = cur_branch->next;
		} while (cur_branch != cas_branch);
		break;
	case select_STATEMENT:
		UNPACK_SQL_STATEMENT(select, v, select);
		// SqlColumnListAlias that is a linked list
		result |= populate_data_type_column_list_alias(select->select_list, &child_type1, TRUE, cursorId);
		*type = child_type1;
		// SqlJoin
		result |= populate_data_type(select->table_list, &child_type1, cursorId);
		// SqlValue (?)
		result |= populate_data_type(select->where_expression, &child_type1, cursorId);
		// SqlColumnListAlias that is a linked list
		result |= populate_data_type_column_list_alias(select->group_by_expression, &child_type1, TRUE, cursorId);
		// SqlValue (?)
		result |= populate_data_type(select->having_expression, &child_type1, cursorId);
		// SqlColumnListAlias that is a linked list
		result |= populate_data_type_column_list_alias(select->order_by_expression, &child_type1, TRUE, cursorId);
		break;
	case function_call_STATEMENT:
		// TODO: we will need to know the return types of functions to do this
		// For now, just say STRING_LITERAL
		UNPACK_SQL_STATEMENT(function_call, v, function_call);
		// SqlColumnList
		result |= populate_data_type_column_list(function_call->parameters, type, TRUE, cursorId);
		*type = STRING_LITERAL;
		break;
	case aggregate_function_STATEMENT:
		UNPACK_SQL_STATEMENT(aggregate_function, v, aggregate_function);
		// SqlColumnList : We have only one parameter to aggregate functions so no loop needed hence FALSE used below.
		result |= populate_data_type_column_list(aggregate_function->parameter, type, FALSE, cursorId);
		// Note that COUNT(...) is always an INTEGER type even though ... might be a string type column.
		// Hence the if check below.
		switch(aggregate_function->type) {
		case COUNT_ASTERISK_AGGREGATE:
		case COUNT_AGGREGATE:
		case COUNT_AGGREGATE_DISTINCT:
			*type = INTEGER_LITERAL;
			break;
		case AVG_AGGREGATE:
		case AVG_AGGREGATE_DISTINCT:
		case SUM_AGGREGATE:
		case SUM_AGGREGATE_DISTINCT:
			if ((STRING_LITERAL == *type) || (BOOLEAN_VALUE == *type)) {
				/* STRING or BOOLEAN type cannot be input for the AVG or SUM function so signal
				 * an error in that case.
				 */
				ERROR(ERR_MISTYPED_FUNCTION, get_aggregate_func_name(aggregate_function->type),
								get_user_visible_type_string(*type));
				yyerror(NULL, NULL, &aggregate_function->parameter, NULL, cursorId, NULL);
				result = 1;
			}
			break;
		case MIN_AGGREGATE:
		case MAX_AGGREGATE:
			if (BOOLEAN_VALUE == *type) {
				/* BOOLEAN type cannot be input for the MIN or MAX function so signal an error in that case. */
				ERROR(ERR_MISTYPED_FUNCTION, get_aggregate_func_name(aggregate_function->type),
								get_user_visible_type_string(*type));
				yyerror(NULL, NULL, &aggregate_function->parameter, NULL, cursorId, NULL);
				result = 1;
				break;
			}
			/* In this case, *type should be the same as the type of "aggregate_function->parameter"
			 * which is already set above so copy that over for later use in physical plan.
			 */
			aggregate_function->param_type = *type;
			break;
		default:
			assert(FALSE);
			break;
		}
		break;
	case join_STATEMENT:
		UNPACK_SQL_STATEMENT(start_join, v, join);
		cur_join = start_join;
		do {
			result |= populate_data_type(cur_join->value, type, cursorId);
			result |= populate_data_type(cur_join->condition, type, cursorId);
			cur_join = cur_join->next;
		} while(cur_join != start_join);
		break;
	case value_STATEMENT:
		UNPACK_SQL_STATEMENT(value, v, value);
		switch(value->type) {
		case CALCULATED_VALUE:
			result |= populate_data_type(value->v.calculated, &child_type1, cursorId);
			*type = child_type1;
			break;
		case BOOLEAN_VALUE:
		case NUMERIC_LITERAL:
		case INTEGER_LITERAL:
		case STRING_LITERAL:
		case NUL_VALUE:
		case FUNCTION_NAME:
		case PARAMETER_VALUE:	   // Note: This is a possibility in "populate_data_type" but not in "hash_canonical_query"
		case UNKNOWN_SqlValueType: // Note: This is a possibility in "populate_data_type" but not in "hash_canonical_query"
			*type = value->type;
			break;
		case COLUMN_REFERENCE:
			/* If this happens it probably means it wasn't an extended reference
			 * which is not something we want to happen, the parser should expand
			 * all column references to be fully qualified
			 */
			assert(FALSE);
			result = 1;
			break;
		case COERCE_TYPE:
			result |= populate_data_type(value->v.coerce_target, &child_type1, cursorId);
			/* Note down type of target before coerce */
			value->pre_coerced_type = child_type1;
			/* At this time (Jan 2020), we allow any type to be coerced to any other type at parser time.
			 * Errors in type conversion, if any, should show up at run-time based on the actual values.
			 * But since our run-time is M and we currently only allow INTEGER/NUMERIC/STRING types,
			 * M will allow for converting between either of these types without any errors which is
			 * different from Postgres (where `'Zero'::integer` will cause an error). We will deal with
			 * this if users complain about this incompatibility with Postgres.
			 */
			*type = value->coerced_type;
			break;
		default:
			assert(FALSE);
			ERROR(ERR_UNKNOWN_KEYWORD_STATE, "");
			break;
		}
		break;
	case column_alias_STATEMENT:
		if (column_list_alias_STATEMENT == v->v.column_alias->column->type) {
			result |= populate_data_type(v->v.column_alias->column, type, cursorId);
		} else {
			UNPACK_SQL_STATEMENT(column, v->v.column_alias->column, column);
			switch(column->type) {
			case BOOLEAN_TYPE:
				*type = BOOLEAN_VALUE;
				break;
			case INTEGER_TYPE:
				*type = INTEGER_LITERAL;
				break;
			case NUMERIC_TYPE:
				*type = NUMERIC_LITERAL;
				break;
			case STRING_TYPE:
				*type = STRING_LITERAL;
				break;
			case UNKNOWN_SqlDataType:
				// This could be a column that came in from a sub-query before when the sub-query column
				// was qualified (in "qualify_statement.c"). But by now we would have finished qualifying
				// all columns so we should be able to find out the column type at this point. Fix it now.
				assert(NULL != column->pre_qualified_cla);
				result |= populate_data_type(column->pre_qualified_cla->column_list, type, cursorId);
				switch(*type) {
				case BOOLEAN_VALUE:
					column->type = BOOLEAN_TYPE;
					break;
				case INTEGER_LITERAL:
					column->type = INTEGER_TYPE;
					break;
				case NUMERIC_LITERAL:
					column->type = NUMERIC_TYPE;
					break;
				case STRING_LITERAL:
					column->type = STRING_TYPE;
					break;
				case NUL_VALUE:
					/* NULL values need to be treated as some known type. We choose STRING_TYPE
					 * as this corresponds to the TEXT type of postgres to be compatible with it.
					 * See https://doxygen.postgresql.org/parse__coerce_8c.html#l01373 for more details.
					 */
					column->type = STRING_TYPE;
					break;
				default:
					assert(FALSE);
					break;
				}
				break;
			default:
				assert(FALSE);
				ERROR(ERR_UNKNOWN_KEYWORD_STATE, "");
				result = 1;
				break;
			}
		}
		break;
	case column_list_STATEMENT:
		// Note: We do not loop through the list (just like is done in "hash_canonical_query")
		result |= populate_data_type_column_list(v, type, FALSE, cursorId);
		break;
	case column_list_alias_STATEMENT:
		// Note: We do not loop through the list (just like is done in "hash_canonical_query")
		result |= populate_data_type_column_list_alias(v, type, FALSE, cursorId);
		break;
	case table_STATEMENT:
		// Do nothing; we got here through a table_alias
		break;
	case table_alias_STATEMENT:
		UNPACK_SQL_STATEMENT(table_alias, v, table_alias);
		result |= populate_data_type(table_alias->table, type, cursorId);
		assert((select_STATEMENT != table_alias->table->type)
			|| (table_alias->table->v.select->select_list == table_alias->column_list));
		if (select_STATEMENT != table_alias->table->type)
			result |= populate_data_type(table_alias->column_list, &child_type1, cursorId);
		break;
	case binary_STATEMENT:
		UNPACK_SQL_STATEMENT(binary, v, binary);
		result |= populate_data_type(binary->operands[0], &child_type1, cursorId);
		if (((BOOLEAN_IN == binary->operation) || (BOOLEAN_NOT_IN == binary->operation))
			&& (column_list_STATEMENT == binary->operands[1]->type))
		{	// SqlColumnList
			result |= populate_data_type_column_list(binary->operands[1], &child_type2, TRUE, cursorId);
		} else {
			// SqlStatement (?)
			result |= populate_data_type(binary->operands[1], &child_type2, cursorId);
		}
		if ((PARAMETER_VALUE == child_type1) || (NUL_VALUE == child_type1)) {
			child_type1 = child_type2;
		} else if ((PARAMETER_VALUE == child_type2) || (NUL_VALUE == child_type2)) {
			child_type2 = child_type1;
		} else if ((INTEGER_LITERAL == child_type1) && (NUMERIC_LITERAL == child_type2)){
			child_type1 = child_type2;
		} else if ((INTEGER_LITERAL == child_type2) && (NUMERIC_LITERAL == child_type1)){
			child_type2 = child_type1;
		}
		switch(binary->operation) {
		case ADDITION:
		case SUBTRACTION:
		case DIVISION:
		case MULTIPLICATION:
		case MODULO:
			if (!result) {
				if ((INTEGER_LITERAL != child_type1) && (NUMERIC_LITERAL != child_type1)) {
					ERROR(ERR_TYPE_NOT_COMPATIBLE, get_user_visible_type_string(child_type1),	\
												"arithmetic operations");
					yyerror(NULL, NULL, &binary->operands[0], NULL, NULL, NULL);
					result = 1;
				}
				if ((INTEGER_LITERAL != child_type2) && (NUMERIC_LITERAL != child_type2)) {
					ERROR(ERR_TYPE_NOT_COMPATIBLE, get_user_visible_type_string(child_type2),	\
												"arithmetic operations");
					yyerror(NULL, NULL, &binary->operands[1], NULL, NULL, NULL);
					result = 1;
				}
			}
			*type = child_type1;
			break;
		case CONCAT:
			/* Postgres allows || operator as long as at least one operand is STRING type. Else it issues an error.
			 * Do the same in Octo for compatibility.
			 */
			if ((STRING_LITERAL != child_type1) && (STRING_LITERAL != child_type2)) {
				if (!result) {
					ERROR(ERR_TYPE_NOT_COMPATIBLE, get_user_visible_type_string(child_type1), "|| operator");
					yyerror(NULL, NULL, &binary->operands[0], NULL, NULL, NULL);
					ERROR(ERR_TYPE_NOT_COMPATIBLE, get_user_visible_type_string(child_type2), "|| operator");
					yyerror(NULL, NULL, &binary->operands[1], NULL, NULL, NULL);
					result = 1;
				}
			} else {
				SqlStatement	**target;
				SqlValueType	child_type;

				/* If one operand is BOOLEAN_VALUE, add type cast operator (::string) to it.
				 * Not needed for INTEGER_LITERAL or NUMERIC_LITERAL as M handles this fine.
				 */
				if (BOOLEAN_VALUE == child_type1) {
					assert(BOOLEAN_VALUE != child_type2);
					child_type = child_type1;
					target = &binary->operands[0];
				} else if (BOOLEAN_VALUE == child_type2) {
					assert(BOOLEAN_VALUE != child_type1);
					child_type = child_type2;
					target = &binary->operands[1];
				} else {
					target = NULL;
				}
				if (NULL != target) {
					SqlStatement	*sql_stmt;

					SQL_STATEMENT(sql_stmt, value_STATEMENT);
					MALLOC_STATEMENT(sql_stmt, value, SqlValue);
					UNPACK_SQL_STATEMENT(value, sql_stmt, value);
					value->type = COERCE_TYPE;
					value->coerced_type = STRING_LITERAL;
					value->pre_coerced_type = child_type;
					value->v.coerce_target = *target;
					*target = sql_stmt;
				}
			}
			child_type1 = child_type2 = *type = STRING_LITERAL;
			break;
		case BOOLEAN_OR:
		case BOOLEAN_AND:
			if (!result) {
				if (BOOLEAN_VALUE != child_type1) {
					ERROR(ERR_TYPE_NOT_COMPATIBLE, get_user_visible_type_string(child_type1),	\
												"boolean operations");
					yyerror(NULL, NULL, &binary->operands[0], NULL, NULL, NULL);
					result = 1;
				}
				if (BOOLEAN_VALUE != child_type2) {
					ERROR(ERR_TYPE_NOT_COMPATIBLE, get_user_visible_type_string(child_type2),	\
												"boolean operations");
					yyerror(NULL, NULL, &binary->operands[1], NULL, NULL, NULL);
					result = 1;
				}
			}
			*type = BOOLEAN_VALUE;
			break;
		default:
			*type = BOOLEAN_VALUE;
			break;
		}
		if (!result && (child_type1 != child_type2)) {
			int	i;

			ERROR(ERR_TYPE_MISMATCH, get_user_visible_type_string(child_type1),
							get_user_visible_type_string(child_type2));
			for (i = 0; i < 2; i++) {
				yyerror(NULL, NULL, &binary->operands[i], NULL, NULL, NULL);
			}
			result = 1;
		}
		break;
	case set_operation_STATEMENT:
		UNPACK_SQL_STATEMENT(set_operation, v, set_operation);
		result |= populate_data_type(set_operation->operand[0], &child_type1, cursorId);
		*type = child_type1;
		result |= populate_data_type(set_operation->operand[1], &child_type2, cursorId);
		/* Now that the types of operands to the SET operation have been populated, do some more checks of
		 * whether the # and types of columns on both operands match. If not issue error.
		 */
		{
			SqlTableAlias		*table_alias[2];
			SqlColumnListAlias	*cur_cla[2], *start_cla[2];
			SqlStatement		*sql_stmt;
			boolean_t		terminate_loop[2] = {FALSE, FALSE};
			SqlColumnListAlias	*type_mismatch_cla[2] = {NULL, NULL};
			SqlColumnListAlias	*cur_set_cla, *start_set_cla;
			SqlSetOperation		*set_operand;
			int			i;

			for (i = 0; i < 2; i++) {
				sql_stmt = set_operation->operand[i];
				if (table_alias_STATEMENT == sql_stmt->type) {
					UNPACK_SQL_STATEMENT(table_alias[i], sql_stmt, table_alias);
					assert(NULL != table_alias[i]->column_list);
					UNPACK_SQL_STATEMENT(start_cla[i], table_alias[i]->column_list, column_list_alias);
				} else {
					assert(set_operation_STATEMENT == sql_stmt->type);
					UNPACK_SQL_STATEMENT(set_operand, sql_stmt, set_operation);
					start_cla[i] = set_operand->col_type_list;
				}
				assert(NULL != start_cla[i]);
				cur_cla[i] = start_cla[i];
			}
			assert(NULL == set_operation->col_type_list);
			start_set_cla = NULL;
			do {
				SqlValueType	left_type, right_type;
				boolean_t	is_type_mismatch;

				left_type = cur_cla[0]->type;
				right_type = cur_cla[1]->type;
				/* Assert all possible valid types. This is used to simplify the `if` checks below
				 * that determine the value of `is_type_mismatch`.
				 */
				assert((BOOLEAN_VALUE == left_type) || (INTEGER_LITERAL == left_type)
					|| (NUMERIC_LITERAL == left_type) || (STRING_LITERAL == left_type)
					|| (NUL_VALUE == left_type));
				assert((BOOLEAN_VALUE == right_type) || (INTEGER_LITERAL == right_type)
					|| (NUMERIC_LITERAL == right_type) || (STRING_LITERAL == right_type)
					|| (NUL_VALUE == right_type));
				/* If not yet found any type mismatch, check for one. If already found one, keep just that.
				 * In general, all types are compatible with only themselves.
				 * Exception is that
				 *	a) NUMERIC and INTEGER are compatible with each other and no other type.
				 *	b) NULL is compatible with any type.
				 */
				if (NULL == type_mismatch_cla[0]) {
					switch(left_type) {
					case BOOLEAN_VALUE:
						is_type_mismatch = ((NUL_VALUE != right_type) && (BOOLEAN_VALUE != right_type));
						break;
					case INTEGER_LITERAL:
					case NUMERIC_LITERAL:
						is_type_mismatch = ((NUL_VALUE != right_type) && (INTEGER_LITERAL != right_type)
									&& (NUMERIC_LITERAL != right_type));
						break;
					case STRING_LITERAL:
						is_type_mismatch = ((NUL_VALUE != right_type) && (STRING_LITERAL != right_type));
						break;
					case NUL_VALUE:
						is_type_mismatch = FALSE;
						break;
					default:
						assert(FALSE);
						FATAL(ERR_UNKNOWN_KEYWORD_STATE, "");
						break;
					}
					if (is_type_mismatch) {
						/* Record the first type mismatch location */
						type_mismatch_cla[0] = cur_cla[0];
						type_mismatch_cla[1] = cur_cla[1];
					}
				}
				/* Construct `column_list` for `set_operation` (needed by caller `populate_data_type`) */
				OCTO_CMALLOC_STRUCT(cur_set_cla, SqlColumnListAlias);
				if (NUL_VALUE != left_type) {
					/* Left side column is not NULL, inherit that type for outer SET operation */
					*cur_set_cla = *cur_cla[0];
				} else {
					/* Left side column is NULL, inherit right side column type
					 * for outer SET operation.
					 */
					*cur_set_cla = *cur_cla[1];
				}
				dqinit(cur_set_cla);
				if (NULL != start_set_cla) {
					dqappend(start_set_cla, cur_set_cla);
				} else {
					start_set_cla = cur_set_cla;
				}
				for (i = 0; i < 2; i++) {
					cur_cla[i] = cur_cla[i]->next;
					if (cur_cla[i] == start_cla[i])
						terminate_loop[i] = TRUE;
				}
			} while (!terminate_loop[0] && !terminate_loop[1]);
			if (terminate_loop[0] != terminate_loop[1]) {
				// The # of columns in the two operands (of the SET operation) do not match. Issue error.
				ERROR(ERR_SETOPER_NUMCOLS_MISMATCH, get_set_operation_string(set_operation->type));
				location = ((!terminate_loop[0]) ? cur_cla[0]->column_list->loc : cur_cla[1]->column_list->loc);
				yyerror(&location, NULL, NULL, NULL, cursorId, NULL);
				result = 1;
			} else if (NULL != type_mismatch_cla[0]) {
				// The type of one column in the two operands (of the SET operation) do not match. Issue error.
				ERROR(ERR_SETOPER_TYPE_MISMATCH, get_set_operation_string(set_operation->type),		\
					get_user_visible_type_string(type_mismatch_cla[0]->type),			\
					get_user_visible_type_string(type_mismatch_cla[1]->type));
				location = type_mismatch_cla[0]->column_list->loc;
				yyerror(&location, NULL, NULL, NULL, cursorId, NULL);
				location = type_mismatch_cla[1]->column_list->loc;
				yyerror(&location, NULL, NULL, NULL, cursorId, NULL);
				result = 1;
			}
			assert(NULL != start_set_cla);
			set_operation->col_type_list = start_set_cla;
		}
		break;
	case unary_STATEMENT:
		UNPACK_SQL_STATEMENT(unary, v, unary);
		result |= populate_data_type(unary->operand, &child_type1, cursorId);
		/* Check for type mismatches */
		if (((FORCE_NUM == unary->operation) || (NEGATIVE == unary->operation))
				&& ((INTEGER_LITERAL != child_type1) && (NUMERIC_LITERAL != child_type1))) {
			/* Unary + and - operators cannot be used on non-numeric or non-integer types */
			ERROR(ERR_INVALID_INPUT_SYNTAX, get_user_visible_type_string(child_type1));
			yyerror(NULL, NULL, &unary->operand, NULL, NULL, NULL);
			result = 1;
			break;
		}
		/* If the unary operation is EXISTS, then set the type of the result to BOOLEAN,
		 * not to the type inherited from the sub-query passed to EXISTS.
		 */
		*type = ((BOOLEAN_EXISTS == unary->operation) ? BOOLEAN_VALUE : child_type1);
		assert((BOOLEAN_NOT != unary->operation) || (BOOLEAN_VALUE == *type));
		break;
	default:
		assert(FALSE);
		ERROR(ERR_UNKNOWN_KEYWORD_STATE, "");
		result = 1;
		break;
	}
	return result;
}
