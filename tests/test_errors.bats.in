#################################################################
#								#
# Copyright (c) 2019-2020 YottaDB LLC and/or its subsidiaries.	#
# All rights reserved.						#
#								#
#	This source code contains the intellectual property	#
#	of its copyright holder(s), and is made available	#
#	under a license.  If you do not know the terms of	#
#	the license, please stop and do not read further.	#
#								#
#################################################################

load test_helpers

setup() {
  init_test
  createdb
  load_fixture names.sql
  load_fixture names.zwr
  load_fixture default_user.zwr
}

@test "TERR001 : Invalid Octo GLD" {
  export ydb_gbldir="does_not_exist.gld"
  octo <<OCTO 2>&1 | tee output.txt
SELECT * FROM names WHERE lastName = "Cool";
OCTO
  verify_output TERR001 output.txt noinfo nodebug
}

@test "TERR002 : Octo can recover from error (e.g. DIVBYZERO)" {
  octo <<OCTO 2>&1 | tee output.txt
select 1/0 from names;
select * from names;
OCTO
  verify_output TERR002 output.txt noinfo nodebug
}

@test "TERR003 : Octo issues FATAL error for missing input file" {
  octo -f missing.sql 2>&1 | tee output.txt
  verify_output TERR003 output.txt noinfo nodebug
}

@test "Octo doesn't generate core file for fatal errors" {
  octo -f missing.sql 2>&1 | tee output.txt
  verify_output TERR003 output.txt noinfo nodebug
  run find "core*"
  [[ $status -eq 1 ]]
}

@test "TERR004 : Rocto can recover from error (e.g. DIVBYZERO)" {
  test_port=$(start_rocto 1443)
  run_psql $test_port <<OCTO 2>&1 | tee output.txt
select 1/0 from names;
select * from names;
OCTO
  stop_rocto
  verify_output TERR004 output.txt noinfo nodebug
}

@test "TERR008 : Test inputs with tabs are underlined correctly" {
  # this input contains  mix of spaces a tabs
  octo -vv <<OCTO 2>&1 | tee output.txt
	   select
	notathing  	  from
			names
	;
OCTO
  verify_output TERR008 output.txt
}

@test "TERR009 : Test syntax error" {
  cat >>input.sql<<CAT
  !;
CAT
  octo -f input.sql 2>&1 | tee output.txt
  verify_output TERR009 output.txt
}

@test "TERR010 : Rocto prefixes query errors in its log" {
  test_port=$(start_rocto 1337)
  cat <<CAT >> input.sql
select -((select 1 from names limit 1)+(select 2 from names limit 1))+3;
select ABS(-id)+(select firstname from names limit 1) as absid from names;
SELECT  A.firstName, (SELECT B.firstName FROM NAMES LIMIT 1) FROM NAMES A INNER JOIN NAMES B on (A.firstName = B.firstName);
SELECT  A.firstName, (SELECT C.firstName FROM NAMES AS C WHERE C.firstName = B.firstName LIMIT 1) FROM NAMES A INNER JOIN NAMES B on (A.firstName = B.firstName);
SELECT  A.id,A.firstName,B.id,B.firstName,(SELECT C.firstName FROM NAMES AS C WHERE C.firstName = B.firstName LIMIT 1) FROM NAMES A INNER JOIN NAMES B on (A.firstName = B.firstName);
SELECT  A.id,A.firstName,B.id,B.firstName,(SELECT C.firstName FROM NAMES AS C WHERE C.firstName = B.firstName) FROM NAMES A INNER JOIN NAMES B on (A.firstName = B.firstName);
select * from names as n1 where 1::boolean;
select * from names as n1 where (select n2.id % 2 != 1 from names n2 where n1.id = n2.id);
select * from names as n1 where (n1.id != 0) AND (select n2.id != 1 from names n2 where n1.id = n2.id);
select * from names as n1 where NOT (select n2.id != 1 from names n2 where n1.id = n2.id);
SELECT t.typname,
       t.oid
FROM   pg_catalog.pg_type t
JOIN   pg_catalog.pg_namespace n
ON     (
              t.typnamespace = n.oid)
WHERE  n.nspname != 'pg_toast'
AND    (
              t.typrelid = 0
       OR
              (
                     SELECT c.relkind = 'c'
                     FROM   pg_catalog.pg_class c
                     WHERE  c.oid = t.typrelid));
CAT
  run_psql $test_port < input.sql 2>&1 | tee output.txt
  stop_rocto
  # needed because the normal nodebug parameter deletes too many lines
  sed -i '/\[DEBUG\]/d' rocto.log
  verify_output TERR010 rocto.log noinfo nowarn psql noforcedhalt
}

@test "TERR011 : queries ensure each table has a unique alias" {
  cat <<CAT >> input.sql
select * from names natural join names;
select * from names as n1 natural join names as n1;
select * from names as n1 natural join names as n2 natural join names as n1;
select * from names as n1 natural join names as n2 join names as n1 on n1.id = n2.id;
select * from names as n1 inner join names as n1 on n1.id = n1.id;
select * from names as n1 left join names as n1 on n1.id = n1.id;
select * from names as n1 right join names as n1 on n1.id = n1.id;
select * from names as n1 join names as n1 on n1.id = n1.id;
select * from names as n1 cross join names as n1;
select * from names n1 inner join names n2 on n1.id = n2.id left join names n2 on n1.id = n2.id;
select * from names n1 left join names n2 on n1.id = n2.id inner join names n2 on n1.id = n2.id;
select * from names n1 right join names n2 on n1.id = n2.id left join names n2 on n1.id = n2.id;
select * from names n1 right join names n2 on n1.id = n2.id natural join names n2;
select * from names, names;
select * from names as n1, names as n1;
select * from names as n1, names as n2 inner join names n3 on n1.id = n2.id left join names n2 on n1.id = n2.id;
select * from names as n1 left join names as n1 on n1.id = n1.id left join names as n3 on n1.id = n3.id;
select * from names left join names names on n1.id = names.id;
select * from names left join nameswithages names on n1.id = names.id;
select * from ((select * from names union select * from names) union (select * from names union select * from names)) n1 left join names n1 on n1.id = n1.id;
select * from (((select * from names union select * from names) union (select * from names union select * from names)) union ((select * from names union select * from names) union (select * from names union select * from names))) n1 left join names n1 on n1.id = n1.id;


-- Test that table name is no longer usable once an alias has been assigned to it
select * from names n1 left join nameswithages names on n1.id = names.id;

CAT
  cat input.sql >> output.txt
  octo -f input.sql 2>&1 | tee -a output.txt
  verify_output TERR011 output.txt
}

@test "TERR012 : ydb_routines env var not set correctly" {
  export ydb_routines=""
  octo <<OCTO 2>&1 | tee output.txt
SELECT * FROM names WHERE lastName = "Cool";
OCTO
  export ydb_routines=". $ydb_dist"
  octo <<OCTO 2>&1 | tee -a output.txt
SELECT * FROM names WHERE lastName = "Cool";
OCTO
  verify_output TERR012 output.txt noinfo nodebug
}

@test "TERR013 : Issue error when column name in query is ambiguous (due to multiple columns with same name)" {
  load_fixture TERR013.sql subtest novv
  verify_output TERR013 output.txt
}

@test "TERR014 : Error underline is correct when there are extra newlines between queries (only possible with octo -f)" {
  load_fixture TERR014.sql subtest novv
  verify_output TERR014 output.txt
}

