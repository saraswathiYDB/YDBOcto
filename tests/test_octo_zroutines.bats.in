#################################################################
#								#
# Copyright (c) 2019 YottaDB LLC and/or its subsidiaries.	#
# All rights reserved.						#
#								#
#	This source code contains the intellectual property	#
#	of its copyright holder(s), and is made available	#
#	under a license.  If you do not know the terms of	#
#	the license, please stop and do not read further.	#
#								#
#################################################################

load test_helpers

setup() {
  init_test
  createdb
  load_fixture names.sql
  load_fixture names.zwr
}

@test "try no octo_zroutines with directory as first element in \$zroutines" {
  mkdir dirA
  export ydb_routines="$(pwd)/dirA $ydb_routines"
  octo <<OCTO 2>&1 | tee output.txt
select * from names limit 1;
OCTO
  echo "Listing of dirA should see generated files" | tee -a output.txt
  ls dirA | tee -a output.txt
  verify_output TOZ000 output.txt
}

@test "try no octo_zroutines with directory as second element in \$zroutines" {
  mkdir dirA
  export ydb_routines="@PROJECT_BINARY_DIR@/src/_ydbocto.so $(pwd)/dirA $ydb_routines"
  octo <<OCTO 2>&1 | tee output.txt
select * from names limit 1;
OCTO
  echo "Listing of dirA should see generated files" | tee -a output.txt
  ls dirA | tee -a output.txt
  verify_output TOZ001 output.txt
}

@test "try no octo_zroutines with object(source) syntax in \$zroutines" {
  mkdir dirA dirB
  export ydb_routines="$(pwd)/dirA($(pwd)/dirB) $ydb_routines"
  octo <<OCTO 2>&1 | tee output.txt
select * from names limit 1;
OCTO
  echo "Listing of dirA should see generated object file" | tee -a output.txt
  ls dirA | tee -a output.txt
  echo "Listing of dirB should see generated M file" | tee -a output.txt
  ls dirB | tee -a output.txt
  verify_output TOZ002 output.txt
}

@test "try no octo_zroutines with no valid directories in \$zroutines" {
  export ydb_routines="@PROJECT_BINARY_DIR@/src/_ydbocto.so"
  octo <<OCTO 2>&1 | tee output.txt
select * from names limit 1;
OCTO
  verify_output TOZ003 output.txt
}

@test "try with octo_zroutines set and in \$zroutines" {
  mkdir dirA
  cat >>octo.conf <<OCTO
octo_zroutines = "$(pwd)/dirA"
OCTO
  export ydb_routines="$(pwd)/dirA $ydb_routines"
  octo -vv <<OCTO 2>&1 | tee output.txt
select * from names limit 1;
OCTO
  echo "Listing of dirA should see generated files" | tee -a output.txt
  ls dirA | tee -a output.txt
  # Verify actual output
  verify_output TOZ004 output.txt noinfo nodebug
  # Verify ydb_routines is properly set
  expect_ydb_routines="$(pwd)/dirA $ydb_routines"
  actual_ydb_routines=`grep ydb_routines output.txt | sed 's/.*ydb_routines="//g;s/"$//g'`
  echo "$expect_ydb_routines" > expect_ydb_routines.txt	# store it for debugging in case == test below fails
  echo "$actual_ydb_routines" > actual_ydb_routines.txt	# store it for debugging in case == test below fails
  [[ "$expect_ydb_routines" == "$actual_ydb_routines" ]]
}

@test "try with octo_zroutines set but not in \$zroutines" {
  mkdir dirA
  cat >>octo.conf <<OCTO
octo_zroutines = "$(pwd)/dirA"
OCTO
  octo <<OCTO 2>&1 | tee output.txt
select * from names limit 1;
OCTO
  echo "Listing of dirA should see generated files" | tee -a output.txt
  ls dirA | tee -a output.txt
  verify_output TOZ005 output.txt
}

@test "use .*() as first element in \$zroutines" {
  mkdir dirA
  export ydb_routines=".*() $(pwd)/dirA $ydb_routines"
  octo <<OCTO 2>&1 | tee output.txt
select * from names limit 1;
OCTO
  echo "Listing of dirA should see generated files" | tee -a output.txt
  ls dirA | tee -a output.txt
  verify_output TOZ006 output.txt
}

@test "use a non-existent directory as first element in \$zroutines" {
  export ydb_routines="$(pwd)/dirA $ydb_routines"
  octo <<OCTO 2>&1 | tee output.txt
select * from names limit 1;
OCTO
  grep "YottaDB error: 150374634,(SimpleAPI),%YDB-E-ZROSYNTAX" output.txt
}

@test "use a non-existent directory as first element in octo_zroutines" {
  cat >>octo.conf <<OCTO
octo_zroutines = "$(pwd)/dirA"
OCTO
  octo <<OCTO 2>&1 | tee output.txt
select * from names limit 1;
OCTO
  sed -i 's/found: \/.*\/dirA/found: PATHTO_dirA/' output.txt
  verify_output TOZ007 output.txt
}

@test "use a regular file as value of octo_zroutines" {
  cat >>octo.conf <<OCTO
octo_zroutines = "$(pwd)/fileA"
OCTO
  touch fileA
  octo <<OCTO 2>&1 | tee output.txt
select * from names limit 1;
OCTO
  sed -i 's/directory: \/.*\/fileA/PATHTO_fileA/' output.txt
  verify_output TOZ008 output.txt
}

@test "use object(source) syntax as value of octo_zroutines" {
  mkdir dirA dirB
  cat >> octo.conf <<OCTO
octo_zroutines = "$(pwd)/dirA($(pwd)/dirB)"
OCTO
  octo <<OCTO 2>&1 | tee output.txt
select * from names limit 1;
OCTO
  echo "Listing of dirA should see generated object file" | tee -a output.txt
  ls dirA | tee -a output.txt
  echo "Listing of dirB should see generated M file" | tee -a output.txt
  ls dirB | tee -a output.txt
  verify_output TOZ009 output.txt
}


@test "set \$zroutines entirely though octo_zroutines" {
  cat >>octo.conf <<OCTO
octo_zroutines = "$ydb_routines"
OCTO
  unset ydb_routines
  unset gtmroutines
  octo <<OCTO 2>&1 | tee output.txt
select * from names limit 1;
OCTO
  verify_output TOZ010 output.txt
}
