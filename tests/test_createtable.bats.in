#################################################################
#								#
# Copyright (c) 2019-2020 YottaDB LLC and/or its subsidiaries.	#
# All rights reserved.						#
#								#
#	This source code contains the intellectual property	#
#	of its copyright holder(s), and is made available	#
#	under a license.  If you do not know the terms of	#
#	the license, please stop and do not read further.	#
#								#
#################################################################

load test_helpers

setup() {
  init_test
  createdb
  load_fixture names.sql
  load_fixture names.zwr
  load_fixture names4.zwr
}

@test "TC001 : create a table so large it can't fit in the buffer" {
  # Generate a file with the mapping
  cat <<OCTO > table1.sql
CREATE TABLE table1 (
  id INTEGER PRIMARY KEY
  $(for i in $(seq 1 1000); do echo ", column$i VARCHAR"; done)
);
OCTO
  # Load the table schema into octo
  octo -f table1.sql >& output.txt
  # Verify that the table created above exists by doing a SELECT (will return no rows since it does not have data).
  octo <<OCTO 2>&1 | tee -a output.txt
SELECT * from table1;
OCTO
  verify_output TC001 output.txt noinfo nodebug
}

@test "TC001A : create table with key in extract" {
  octo <<OCTO 2>&1 | tee output.txt
CREATE TABLE namesE(
  id INTEGER PRIMARY KEY,
  firstName VARCHAR EXTRACT "^names(keys(""id""))",
  lastName VARCHAR
) GLOBAL "^names(keys(""id""))";
OCTO
  run octo <<OCTO
SELECT * FROM namesE;
OCTO
  [ "$status" -eq 0 ]
}

@test "TC001B : create multiple tables in sequence" {
  octo <<OCTO 2>&1 | tee output.txt
CREATE TABLE Customers (CustomerID INTEGER PRIMARY KEY);
CREATE TABLE Orders (OrderID INTEGER PRIMARY KEY);
OCTO
  [ "$?" -eq 0 ]
}

@test "TC002 : create a table which has no keys" {
  octo <<OCTO 2>&1 | tee output.txt
create table names4 (id int, First char(20), Last char(30));
OCTO
  verify_output TC002 output.txt noinfo nodebug
}

@test "TC003 : create a table which has no keys and select from it later" {
  octo <<OCTO 2>&1 | tee output.txt
create table names4 (id int, First char(20), Last char(30));
select * from names4
OCTO
  verify_output TC003 output.txt noinfo nodebug
}

@test "TC004 : recreate a cached table and check that the cached version is deleted" {
  # create a row that will change when more columns are added
  yottadb -run %XCMD 'set ^T1(1)="1|2|3|4"'
  octo <<OCTO 2>&1 | tee output.txt
drop table t1;
create table t1 (a int primary key);
select * from t1;
create table t1 (a int primary key, b int);
select * from t1;
create table t1 (a int primary key, b int, c int);
select * from t1;
create table t1 (a int primary key, b int, c int, d int);
select * from t1;
OCTO
  verify_output TC004 output.txt noinfo nodebug
}

@test "TC005 : create a table with duplicate primary keys then select from it" {
  octo <<OCTO 2>&1 | tee output.txt
drop table t1;
create table t1 (a int primary key, b int primary key);
select * from t1;
OCTO
  verify_output TC005 output.txt noinfo nodebug
}

@test "TC006 : create a table with duplicate key nums then select from it" {
  octo <<OCTO 2>&1 | tee output.txt
drop table t1;
create table t1 (a int primary key, b int key num 1, c int key num 1);
select * from t1;
OCTO
  verify_output TC006 output.txt noinfo nodebug
}

@test "TC007 : create a table with gaps in key nums then select from it" {
  octo <<OCTO 2>&1 | tee output.txt
drop table t1;
create table t1 (a int key num 10, b int key num 5, c int key num 7);
select * from t1;
OCTO
  verify_output TC007 output.txt noinfo nodebug
}

@test "TC008 : create a table with an INTEGER and NUMERIC columns with decimals and integers stored in both" {
  yottadb -run %XCMD 'kill ^T1 set ^T1(1,2.2,3.3)="1|1.1|2|2.2|3|3.3"'
  octo <<OCTO 2>&1 | tee output.txt
drop table t1;
create table t1 (a int primary key, b int, c numeric, d numeric key num 1, e int, f int key num 2);
select * from t1;
OCTO
  verify_output TC008 output.txt
}

@test "TC009 : create a table with INTEGER and NUMERIC column types with strings stored in both" {
  yottadb -run %XCMD 'kill ^T1 set ^T1("hello")="hello|world"'
  octo <<OCTO 2>&1 | tee output.txt
drop table t1;
create table t1(a int primary key, b numeric);
select * from t1;
OCTO
  verify_output TC009 output.txt
}

@test "TC010 : OCTO381 : Allow DATE and TIME types to be specified in the Octo DDL but treated as strings internally" {
  yottadb -run TC010	# load ^datetime global
  load_fixture TC010.sql subtest novv
  # Do not use `verify_output` here as we do not want DATE/TIME substitutions to happen
  # but want to see the actual date/time strings from the ^datetime global displayed in the reference file.
  # That said, there are a few date/time usages that will show up in error message that we want taken out as they
  # correspond to non-deterministic output.
  cp output.txt clean_output.txt
  sed -i 's/[0-9]\{4\}-[0-9]\{2\}-[0-9]\{2\} [0-9]\{2\}:[0-9]\{2\}:[0-9]\{2\}/DATE TIME/g' clean_output.txt
  # Filter file path
  sed -i 's/\/.*\/.*\.[yc]:[0-9]*/PATH:LINENUM/' clean_output.txt
  diff @PROJECT_SOURCE_DIR@/tests/outref/TC010.ref clean_output.txt
}

@test "TC011 : OCTO411 : Add support for INTEGER precision specification to parser for CREATE TABLE statements" {
  load_fixture TC011.sql subtest novv
  verify_output TC011 output.txt
}

