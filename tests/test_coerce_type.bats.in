#################################################################
#								#
# Copyright (c) 2019-2020 YottaDB LLC and/or its subsidiaries.	#
# All rights reserved.						#
#								#
#	This source code contains the intellectual property	#
#	of its copyright holder(s), and is made available	#
#	under a license.  If you do not know the terms of	#
#	the license, please stop and do not read further.	#
#								#
#################################################################

load test_helpers

setup() {
  init_test
  createdb
  load_fixture names.sql
  load_fixture names.zwr
}

@test "TCT000: coerce string to INTEGER" {
  load_fixture TCT000.sql subtest novv
  verify_output TCT000 output.txt
}

@test "TCT001: coerce canonical number string to INTEGER" {
  load_fixture TCT001.sql subtest novv
  verify_output TCT001 output.txt
}

@test "TCT002: coerce NUMERIC type to INTEGER" {
  load_fixture TCT002.sql subtest novv
  verify_output TCT002 output.txt
}

@test "TCT003: coerce string to NUMERIC" {
  load_fixture TCT003.sql subtest novv
  verify_output TCT003 output.txt
}

@test "TCT004 : coerce canonical number string to NUMERIC" {
  load_fixture TCT004.sql subtest novv
  verify_output TCT004 output.txt
}

@test "TCT005 : coerce INTEGER type to INTEGER" {
  load_fixture TCT005.sql subtest novv
  verify_output TCT005 output.txt
}

@test "TCT006 : compare integer and numeric types" {
  load_fixture TCT006.sql subtest novv
  verify_output TCT006 output.txt
}

@test "TCT007 : test coerce of functions" {
  yottadb -run ^%XCMD 'set ^%ydboctoocto("functions","DOLLARZWRITE")="$ZWRITE"'
  load_fixture TCT007.sql subtest novv
  verify_output TCT007 output.txt
}

@test "TCT008 : test 2 coerced functions generate different M files" {
  yottadb -run ^%XCMD 'set ^%ydboctoocto("functions","DOLLARZWRITE")="$ZWRITE"'
  yottadb -run ^%XCMD 'set ^%ydboctoocto("functions","DOLLARREV")="$REVERSE"'
  load_fixture TCT008.sql subtest novv
  echo "Listing of M files, should see 2" | tee -a output.txt
  ls -1 _ydboctoP*.m 2>&1 | tee -a output.txt
  verify_output TCT008 output.txt
}

@test "TCT009 : OCTO394 : Assertion failed in LP_VERIFY_STRUCTURE.C when UNARY + operator is used" {
  load_fixture TCT009.sql subtest novv
  verify_output TCT009 output.txt
}

@test "TCT010 : OCTO437 : DIVZERO error even though query that does no divide by 0 is run with and without :: (type cast operator)" {
  load_fixture TCT010.sql subtest novv
  # Include generated M code in reference file to also verify generated M code
  cat _ydboctoP*.m >> output.txt
  verify_output TCT010 output.txt
}

@test "TCT011 : OCTO304 : Type cast operator (`::`) does not work" {
  $ydb_dist/mumps -run %XCMD 'set ^%ydboctoocto("functions","DOLLARZWRITE")="$ZWRITE"'	# Needed by one query in TCT011.sql
  load_fixture TCT011.sql subtest novv
  # Include generated M code in reference file to also verify generated M code
  cat _ydboctoP*.m >> output.txt
  verify_output TCT011 output.txt
}

@test "TCT012 : OCTO300 : Octo uses right most type in calculated columns rather than highest precision type" {
  load_fixture TCT012.sql subtest novv
  # Include generated M code in reference file to also verify generated M code
  cat _ydboctoP*.m >> output.txt
  verify_output TCT012 output.txt
}

@test "TCT013 : OCTO235 : Rename INT data type to NUMERIC as we do also support floating-point/decimal numbers" {
  # This test has a few queries that use the Northwind schema so load that too.
  load_fixture northwind.sql
  load_fixture northwind.zwr
  load_fixture TCT013.sql subtest novv
  verify_output TCT013 output.txt
}

